/** @type {import('tailwindcss').Config} */

const defaultTheme = require('tailwindcss/defaultTheme');
module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        primary: {
          25: '#F1F8FC',
          50: '#E6F1F7',
          100: '#CCE2F0',
          200: '#B3D4E8',
          300: '#99C5E0',
          400: '#66A8D1',
          500: '#338BC1',
          600: '#006EB2',
          700: '#00588E',
          800: '#00426B',
          900: '#002C47',
        },
        gray: {
          25: '#FCFCFC',
          50: '#FAFAFA',
          100: '#F4F5F5',
          200: '#E2E3E4',
          300: '#C8CACC',
          400: '#A3A6A8',
          500: '#858789',
          600: '#858789',
          700: '#3D454D',
          800: '#232E38',
          900: '#131C26',
        },
        purple: {
          200: '#DAD8F8',
          400: '#9F9ADF',
          700: '#555186',
          900: '#383659',
        },
        turquoise: {
          50: '#EAF5FB',
          100: '#D3E8F3',
          400: '#5092B4',
          600: '#38667E',
          900: '#0B405B',
        },
        red: {
          50: '#FEF2F1',
          400: '#F98880',
          600: '#E03124',
          700: '#C82C1E',
          800: '#99271A',
          900: '#742215',
        },
        yellow: {
          50: '#FEF9DD',
          200: '#FCE96A',
          300: '#FACA15',
          400: '#E3A008',
          700: '#885716',
        },
        green: {
          50: '#EBF9EE',
          400: '#3DC256',
          500: '#179B48',
          700: '#179B48',
        },
        pink: {
          600: '#D61F69',
        },
        orange: {
          400: '#FF8A4C',
        },
      },
      minWidth: {
        80: '80px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms')],
};
