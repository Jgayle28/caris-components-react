# Caris Components React

## Project Goals

To provide Caris with a prebuilt library of reusable components that will scale with all Caris Apps

## Technologies Used

- React
- Tailwind
- Storybook

## How to get started

- After initial download run `yarn` to install dependencies
- To start React server run `yarn start`
- To run Storybook run `yarn storybook`