import "./App.css";
import "./index.css";
function App() {
  return (
    <div className="pt-4 pl-4 text-blue-600 text-4xl font-extrabold">
      React + tailwindcss
    </div>
  );
}

export default App;
