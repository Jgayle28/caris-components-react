import React from 'react';

import { Button } from './Button';
import PlusIcon from '../icons/PlusIcon';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Button',
  component: Button,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => <Button {...args} />;

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  type: 'primary',
  size: 'large',
  label: 'Submit Case',
};

export const Secondary = Template.bind({});
Secondary.args = {
  type: 'secondary',
  size: 'large',
  label: 'Submit Case',
};

export const Ghost = Template.bind({});
Ghost.args = {
  type: 'ghost',
  size: 'large',
  label: 'Submit Case',
};

export const Disabled = Template.bind({});
Disabled.args = {
  type: 'primary',
  size: 'large',
  label: 'Submit Case',
  isDisabled: true,
};

export const Loading = Template.bind({});
Loading.args = {
  type: 'primary',
  size: 'large',
  label: 'Submit Case',
  loading: true,
};

export const IconLeft = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
IconLeft.args = {
  type: 'primary',
  size: 'large',
  label: 'Submit Case',
  iconLeft: <PlusIcon />,
};

export const IconRight = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
IconRight.args = {
  type: 'primary',
  size: 'large',
  label: 'Submit Case',
  iconRight: <PlusIcon />,
};
