import React from 'react';
import PropTypes from 'prop-types';
import './button.css';

/**
 * Caris Button for user interaction
 */
export const Button = ({
  type,
  size,
  label,
  isDisabled,
  loading,
  classNames,
  iconLeft,
  iconRight,
  backgroundColor,
  ...props
}) => {
  // Returns styles for buttons
  const getButtonClasses = () => {
    //Default styles that apply to all buttons
    let classes = [
      'rounded-lg  leading-none transition ease font-medium focus:outline-none focus:ring focus:ring-yellow-300 min-w-80',
    ];

    /**
     * Establish styles based on passed in props
     */
    // types
    if (!isDisabled && type === 'primary')
      classes.push(' ', 'bg-primary-600 text-white hover:bg-primary-900');
    if (!isDisabled && type === 'secondary')
      classes.push(
        ' ',
        'border border-primary-600 text-turquoise-900 hover:bg-turquoise-100'
      );
    if (!isDisabled && type === 'ghost')
      classes.push(' ', 'bg-transparent text-primary-600 hover:bg-gray-200');
    // sizes
    if (size === 'small') classes.push(' ', 'py-2 px-4 text-xs tracking-wider');
    if (size === 'medium') classes.push(' ', 'py-3 px-5 text-sm tracking-wide');
    if (size === 'large')
      classes.push(' ', 'py-4 px-6 text-base tracking-wide');
    // loading
    if (loading && type === 'primary')
      classes.push(
        ' ',
        'bg-primary-600 bg-opacity-25 pointer-events-none flex items-center justify-center'
      );
    if (loading && type === 'secondary')
      classes.push(
        ' ',
        'bg-primary-600 bg-opacity-25 pointer-events-none flex items-center justify-center'
      );
    // disabled
    if (isDisabled)
      classes.push(' ', 'bg-gray-500 text-white cursor-not-allowed');
    if (classNames !== undefined) classes.push(' ', ...classNames);

    // Combine the classes and return them
    return classes.join('');
  };

  const getIconLeftClasses = () => {
    if (iconLeft) {
      if (size === 'medium' || size === 'large') return 'mr-3';
      if (size === 'small') return 'mr-2';
    }
  };
  const getIconRightClasses = () => {
    if (iconRight) {
      if (size === 'medium' || size === 'large') return 'ml-3 h-2.5 w-2.5';
      if (size === 'small') return 'ml-2 h-2 w-2';
    }
  };

  return (
    <button
      type='button'
      className={`${getButtonClasses()}`}
      disabled={isDisabled || loading}
      style={backgroundColor && { backgroundColor }}
      {...props}
    >
      {loading ? (
        <div role='status'>
          <div class='w-4 h-4 border-2 border-primary-900 rounded-full loader'></div>
        </div>
      ) : (
        <div className='flex items-center'>
          <span className={getIconLeftClasses()}>{iconLeft && iconLeft}</span>
          <span>{label}</span>
          <span className={getIconRightClasses()}>
            {iconRight && iconRight}
          </span>
        </div>
      )}
    </button>
  );
};

Button.propTypes = {
  type: PropTypes.string,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  isDisabled: PropTypes.bool,
  loading: PropTypes.bool,
  classNames: PropTypes.string,
  backgroundColor: PropTypes.string,
};
